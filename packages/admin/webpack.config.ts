import {Configuration} from 'webpack';
// noinspection ES6UnusedImports
import { Configuration as WebpackDevServerConfiguration } from 'webpack-dev-server';

import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
// @ts-ignore
import CircularDependencyPlugin from 'circular-dependency-plugin';

import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const distDir = path.join(__dirname, '../../dist');
const distFile = 'renderer.js';

const config: Configuration = {
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },
    
    entry: './src/index.tsx',
    devtool: 'source-map',
    mode: 'development',
    output: {
        path: distDir,
        filename: distFile
    },
    devServer: {
        stats: 'minimal',
        historyApiFallback: {
            disableDotRule: true
        },
        // hot: true,
        hot: false,
        overlay: {
            warnings: true,
            errors: true
        }
    },

    // Add the loader for .ts files.
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    { loader: 'cache-loader' },
                    {
                        loader: 'thread-loader',
                        options: {
                            poolRespawn: false
                        }
                    },
                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            envName: 'development'
                        }
                    },
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                            happyPackMode: true
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(png|svg|jpg|gif|woff2?|eot|ttf)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    },
    plugins: [
        // new HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            title: 'Norbi App Admin'
        }),
        new ForkTsCheckerWebpackPlugin({
            checkSyntacticErrors: true,
        }),
        new CircularDependencyPlugin({
            exclude: /node_modules/,
            failOnError: true
        })
    ]
};

export default config;