import {Observable, BehaviorSubject} from 'rxjs';
import {useState, useEffect, useMemo} from 'react';

type InputFactory<State, Inputs = []> = (inputs$: Observable<Inputs>) => Observable<State>;
type ReturnType<State> = [State, boolean, unknown | undefined, boolean];

export function useObservable<State>(inputFactory: InputFactory<State>): ReturnType<State | undefined>
export function useObservable<State, Inputs extends ReadonlyArray<any>>(inputFactory: InputFactory<State>, initialState: State, inputs: Inputs): ReturnType<State>

export function useObservable<State, Inputs extends ReadonlyArray<any> = []>(
    inputFactory: InputFactory<State, Inputs>,
    initialState?: State,
    inputs: Inputs = [] as unknown as Inputs,
): ReturnType<State | undefined>
{
    const [state, setState] = useState(initialState);
    const [error, setError] = useState<unknown>(undefined);
    const [status, setStatus] = useState<'loading' | 'active' | 'complete'>('loading');

    const inputs$ = useMemo(() => new BehaviorSubject<Inputs>(inputs), []);

    useEffect(() => {
        inputs$.next(inputs);
    }, inputs || []);

    useEffect(
        () => {
            let output$ = inputFactory(inputs$);

            const subscription = output$.subscribe(
                value => setState(value),
                error => setError(error),
                () => setStatus('complete')
            );

            return () => {
                subscription.unsubscribe();
                inputs$.complete();
            };
        },
        [], // immutable forever
    );

    return [state, 'loading' === status, error, 'complete' === status];
}