import React from 'react';
import {BaseComponent} from "@epicentric/utilbelt-react";
import {toast, ToastOptions} from "react-toastify";

interface ToasterConfig {
    message: string;
    type: ToasterType;
}

type ToasterType = 'success' | 'warning' | 'error';

const options: ToastOptions = {
    hideProgressBar: true,
    autoClose: 6000
};

class ToasterService {
    public notify(config: ToasterConfig): void
    {
        switch (config.type) {
            case "error":
                toast.error(config.message, options);
                break;
            case "success":
                toast.success(config.message, options);
                break;
            case "warning":
                toast.warn(config.message, options);
                break;
            default:
                toast(config.message, options);
                break;
        }
    }
}

export const toasterService = new ToasterService;
