import firebase from 'firebase/app';
import 'firebase/firestore';
import {Observable, Observer} from 'rxjs';
import QuerySnapshot = firebase.firestore.QuerySnapshot;

firebase.initializeApp({
    apiKey: "AIzaSyAHQ2wHw-RVPbsjlpI0T-nMZo6KrAmJrPQ",
    authDomain: "norbi-app.firebaseapp.com",
    databaseURL: "https://norbi-app.firebaseio.com",
    projectId: "norbi-app",
    storageBucket: "norbi-app.appspot.com",
    messagingSenderId: "1071414553126",
    appId: "1:1071414553126:web:708013ef9aeeab6cf414e7"
});

export const firestore = firebase.firestore();

export function observableFromQuery(query: firebase.firestore.Query): Observable<QuerySnapshot>
{
    return Observable.create((observer: Observer<QuerySnapshot>) => query.onSnapshot(observer));
}