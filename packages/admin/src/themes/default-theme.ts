import {Theme} from './theme';

export const defaultTheme: Theme = {
    // General
    headerBackgroundColor: 'darkseagreen',
    
    // Sidebar
    sidebarHeaderBackgroundColor: 'green',
    sidebarHoverBackgroundColor: '#1e282c',
    sidebarLinkColor: '#8aa4af',
    sidebarCollapsibleBackgroundColor: '#2A3C40',
    activeSidebarLinkColor: 'white',
    sidebarBackgroundColor: '#222D2F',
    
    // Page
    pageTitleBorderColor: 'black',
};