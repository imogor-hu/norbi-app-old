import {identity} from 'lodash-es';
import {getHeadCellKey, HeadCell, OnSortCallback, SortDirection} from './head-cell';
import {BodyCell, getBodyCellKey} from './body-cell';
import React, {Key, ReactNode} from 'react';
import {styled} from '../../themes/theme';

export interface Column<T>
{
    displayName: string;
    propertyName?: string;
    sortable?: boolean;
    width?: string; // grid-template-columns format, defaults to 1fr
    render?(value: any, item: T, column: Column<T>): ReactNode;
}

export interface SortState<T>
{
    column: Column<T>;
    direction: SortDirection;
}

export interface DataGridProps<T>
{
    columns?: Column<T>[];
    items?: T[];

    itemKey?(item: T): Key;

    /// Sort props
    sort?: SortState<T> | null;
    onSort?: OnSortCallback<T>;
}

export function DataGrid<T>({columns = [], items = [], sort, onSort = identity, itemKey = identity}: DataGridProps<T>)
{
    return <StyledDataGrid style={{gridTemplateColumns: columns.map(column => column.width || '1fr').join(' ')}}>
        {/* Head cells */}
        {columns.map((column, index) => <HeadCell
            onSort={onSort}
            column={column}
            sort={sort
            && sort.column.propertyName === column.propertyName
            && sort.column.displayName === column.displayName
                ? sort.direction
                : null}
            key={getHeadCellKey(column, index)}
        />)}

        {/* Body cells */}
        {items.length
            ? items.map((item, rowIndex) => columns.map((column, columnIndex) => (
                <BodyCell column={column} item={item}
                          key={getBodyCellKey(item, itemKey, column, rowIndex, columnIndex)}/>
            )))
            : <EmptyPlaceholder style={{gridColumn: `span ${columns.length}`}}>
                Nincs megjeleníthető létesítmény
            </EmptyPlaceholder>
        }
    </StyledDataGrid>;
}

const StyledDataGrid = styled.div`
  display: grid;
`;
export const EmptyPlaceholder = styled.div`
  text-align: center;
`;