import {styled} from '../../themes/theme';
import React, {Key} from 'react';
import {getHeadCellKey} from './head-cell';
import {Column} from './data-grid';

interface BodyCellProps<T>
{
    item: T;
    column: Column<T>;
}

export function BodyCell<T>({item, column}: BodyCellProps<T>)
{
    let value: unknown = null;

    if (column.propertyName)
        value = (item as unknown as { [key: string]: unknown })[column.propertyName];
    
    return <StyledBodyCell>
        {column.render ? column.render(value, item, column) : String(value)}
    </StyledBodyCell>
}

export function getBodyCellKey<T>(item: T, itemKey: (item: T) => Key, column: Column<T>, rowIndex: number, columnIndex: number)
{
    const itemId = itemKey(item);
    const columnId = getHeadCellKey(column, columnIndex);
    
    return `${itemId}-${columnId}`;
}

const StyledBodyCell = styled.div`
  padding: 0 .5rem;
`;