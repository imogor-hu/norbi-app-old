import React, {useCallback} from 'react';
import styled from 'styled-components';
import CKEditor, {Editor, EventInfo} from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

interface RichTextEditorProps
{
    placeholder?: string;
    value?: string;
    onChange?: (value: string) => void;
    config?: object;
}

const defaultEditorConfig = {
    toolbar: ['|', 'bold', 'bulletedList'],
};

export function RichTextEditor({onChange, value, config = defaultEditorConfig}: RichTextEditorProps)
{    
    const onChangeCallback = useCallback((event: EventInfo, editor: Editor) => {
        onChange && onChange(editor.getData());
    }, [onChange]);
    
    return <EditorWrapper>
        <CKEditor editor={ClassicEditor}
                  data={value || ''}
                  onChange={onChangeCallback}
                  config={config}>
        </CKEditor>
    </EditorWrapper>;
}

const EditorWrapper = styled.div`
    display: flex;
    width: 100%;
    height: 4.5rem;
`;