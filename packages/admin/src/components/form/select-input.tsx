import React, { ReactNode } from 'react';
import styled from 'styled-components';
import { bind, tracked, BaseComponent } from '@epicentric/utilbelt-react';
import Selectable from '../../models/selectable';
import { FieldProps } from 'formik';
import { get } from 'lodash-es';
import { fromEvent } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export interface SelectInputProps<T extends Selectable>
{
    onChange?: (change: React.ChangeEvent<HTMLInputElement>) => void;
    placeholder?: string;
    selected?: T[]; // The pre-selected item
    values: T[];
    isMulti: boolean; // Is multi-select
    form?: FieldProps;
    getLabel: (item: T) => ReactNode;
}

export interface SelectInputState<T extends Selectable>
{
    items: T[];
    selected: T[];
    isVisible: boolean; // For auto-closing
}

export class SelectInput<T extends Selectable> extends BaseComponent<SelectInputProps<T>, SelectInputState<T>, {}>
{

    // Needed for auto-close
    private ref: React.RefObject<HTMLDivElement>;

    @tracked
    private isVisible: boolean;

    @tracked
    private selected: T[];

    public constructor(props: SelectInputProps<T>, context: {})
    {
        super(props, context);
        // Order: 1. Form, 2. Prop, 3. First element
        this.selected = get(this.props.form, 'field.value')  || this.props.selected || [this.props.values[0]];
        this.isVisible = false;
        this.ref = React.createRef();

        fromEvent(document, 'mousedown')
            .pipe(takeUntil(this.willUnmount))
            .subscribe((event: any) => this.handleClickOutside(event));

    }

    private MultiSelectValue = (data: any) => {
        return <MultiInputValue>
            <span onClick={(e) => this.onItemRemove(e, data.data)}>{this.props.getLabel(data.data)}
                <span>{' '}| X</span>
            </span> 
        </MultiInputValue>;
    };

    private onInputKeyPress(event: React.KeyboardEvent<HTMLInputElement>)
    {
        if (!this.props.isMulti)
            return;

        if (this.selected.length <= 1)
            return;

        // Delete from the selected array to these buttons
        if (event.key === "Backspace" || event.key === "Delete")
            this.selected.pop();
    }

    @bind
    private handleClickOutside(event: MouseEvent)
    {
        // Auto-close
        if (this.ref && !(this.ref.current as Node).contains(event.target as Node))
            this.isVisible = false;
    }

    @bind
    public onWrapperClick(event: React.MouseEvent)
    {
        event.stopPropagation();
        
        this.isVisible = !this.isVisible;
    }

    @bind
    public onListItemClick(event: React.MouseEvent, value: T)
    {
        if (this.props.isMulti)
            this.selected.push(value)
        else
            this.selected = [value];
        
        if (this.props.form)
            this.props.form.form.setFieldValue(this.props.form.field.name, this.selected);
    }

    @bind
    public onItemRemove(event: React.MouseEvent, value: T)
    {
        if (this.selected.length <= 1)
            return;

        let index = this.selected.indexOf(value);
        if (index > -1)
            this.selected.splice(index, 1);

    }

    @bind
    public renderSelected()
    {
        if (this.props.isMulti)
            return this.selected ? this.selected.map((v: any, index: any) => { 
                return <this.MultiSelectValue key={index} data={v} />
            }) : this.props.placeholder;
        else
            return this.selected ? this.props.getLabel(this.selected[0]): this.props.placeholder;
    }
    
    public render()
    {
        return (
        <div ref={this.ref}>
            <SelectWrapper onClick={this.onWrapperClick}>
                <HeaderWrapper>
                    {this.renderSelected()}
                    <InputFieldWrapper>
                        <MultiInputField onKeyDown={(e) => this.onInputKeyPress(e)} />
                    </InputFieldWrapper>
                </HeaderWrapper>
                <ListWrapper>
                    <SelectItemList isVisible={this.isVisible}>
                        {this.props.isMulti ? this.props.values.map((value: any, index: any) => {
                            if (!this.selected.includes(value))
                                return (
                                <ListItem key={index} onClick={(e) => this.onListItemClick(e, value)} >
                                    <span>{this.props.getLabel(value)}</span>
                                </ListItem>);
                        }) : this.props.values.map((value: any, index: any) => {
                            if (this.selected !== value)
                                return (
                                <ListItem key={index} onClick={(e) => this.onListItemClick(e, value)} >
                                    <span>{this.props.getLabel(value)}</span>
                                </ListItem>);
                        })}
                        </SelectItemList>
                </ListWrapper>
            </SelectWrapper>
        </div>
        );
    }

}

const ListItem = styled.div`
    display: flex;
    align-items: center;
    padding: 0 2rem;
    height: 35px;
    border-top: 2px solid gray;
    border-right: 2px solid gray;
    border-left: 2px solid gray;
`;

const ListWrapper = styled.div`
    background: white;
    width: 100%;
`;

const HeaderWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

const InputFieldWrapper = styled.div`
    flex: 1;
`;

const MultiInputField = styled.input`
    background: transparent;
    border: none;
    width: 100%;
    flex: 1;
`;

const MultiInputValue = styled.div`
    display: inline;
    border: .5px solid gray;
    border-radius: 5px;
    padding: 0 2rem;
    margin: 0 1rem;
`;

const SelectWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    flex-wrap: wrap;
    width: 300px;
    height: 35px;
    position: relative;
    border: 2px solid gray;
    border-radius: 5px;
    outline: none;
`;

const SelectItemList = styled.div<{isVisible: boolean}>`
    display: ${props => props.isVisible ? 'block' : 'none'};
    visibility: ${props => props.isVisible ? 'visible' : 'hidden'};
    // overflow: ${props => props.isVisible ? 'auto' : 'hidden'};
    width: inherit;
    position: absolute;
    z-index: 5;
    margin-top: 5px;
    background: inherit;
    border-bottom: 2px solid gray;
    border-radius: 5px;
    outline: none;
    ${props => props.isVisible ? `
    transition: all 0.3s;
    ` : ''};
`;

const InputField = styled.input`
    display: flex;
    color: black;
    background-color: #e4e4e4;
    border: 2px solid transparent;
    border-radius: 5px;
    outline: none;
    width: 60px;
    height: 30px;
    text-align: center;
    font-size: 14px;
    :hover{
        border-color: lightblue;
        transition: .3s linear; 
    }
`;