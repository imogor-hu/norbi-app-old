import {styled} from '../../themes/theme';

export const List = styled.ul`
  padding: 0;
  list-style: none;
`;