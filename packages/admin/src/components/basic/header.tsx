import {styled} from '../../themes/theme';

export const Header = styled.header`
  display: flex;
  align-items: center;
  height: 3rem;
  padding: .5rem 1rem;
  
  background-color: ${props => props.theme.headerBackgroundColor};
`;