import {styled} from '../../themes/theme';
import {StyledIcon} from 'styled-icons/types';
import React, {createContext, ReactNode, useContext, useMemo, useState} from 'react';
import {animated, useSpring} from 'react-spring';
import {KeyboardArrowLeft} from 'styled-icons/material';
import {useMeasure} from '../../utils/use-measure';
import {AccordionContext} from './accordion';
import {identity} from 'lodash-es';

interface CollapsibleProps
{
    // displayed label of the menu element 
    label: string;
    // displayed icon
    icon?: StyledIcon;
    children?: ReactNode;
}

let collapsibleIdCount = 1;

export interface CollapsibleContextValue
{
    setActive(value: boolean, immediate: boolean): void;
}

export const CollapsibleContext = createContext<CollapsibleContextValue>({ setActive: identity });

export function Collapsible({label, icon: Icon, children}: CollapsibleProps)
{
    const accordionContext = useContext(AccordionContext);
    
    const [collapsibleId] = useState(() => collapsibleIdCount++);
    const [localIsOpen, setLocalIsOpen] = useState(false);
    
    const isOpen = accordionContext.exists ? accordionContext.active === collapsibleId : localIsOpen;
    const setIsOpen = useMemo(() => {
        return accordionContext.exists
            ? (value: boolean, animate: boolean) => accordionContext.setActive(value ? collapsibleId : null, animate)
            : setLocalIsOpen; 
    }, [accordionContext.exists]);
    
    const collapsibleContext = useMemo(() => ({ setActive: setIsOpen }), [setIsOpen]);
    
    const [bind, {height}] = useMeasure<HTMLDivElement>();
    const animatedProps = useSpring({
        wrapperHeight: isOpen ? height : 0,
        arrowTransform: isOpen ? 'rotate(-90deg)' : 'rotate(0deg)',
        immediate: accordionContext.exists ? !accordionContext.animate : false
    });

    return <div>
        <CollapsibleElement isActive={isOpen} onClick={() => setIsOpen(!isOpen, true)}>
            {Icon && <Icon/>}
            <CollapsibleLabel>{label}</CollapsibleLabel>
            <AnimatedIcon style={{transform: animatedProps.arrowTransform}}/>
        </CollapsibleElement>

        <CollapsibleWrapper style={{height: animatedProps.wrapperHeight}}>
            <div {...bind}>
                <CollapsibleContext.Provider value={collapsibleContext}>
                    {children}
                </CollapsibleContext.Provider>
            </div>
        </CollapsibleWrapper>
    </div>;
}

const AnimatedIcon = styled(animated(KeyboardArrowLeft))`
  height: 2rem;
  width: 2rem;
`;

const CollapsibleWrapper = styled(animated.div)`
  overflow: hidden;
  background-color: ${props => props.theme.sidebarCollapsibleBackgroundColor};
`;

const CollapsibleElement = styled.div<{ isActive?: boolean }>`
  color: ${props => props.theme.activeSidebarLinkColor};
  cursor: pointer;
  height: 4rem;
  display: flex;
  align-items: center;
  padding: 0 1rem;
  border-left: solid 2px ${props => props.isActive ? props.theme.headerBackgroundColor : 'transparent'};
  
  &:hover {
    background-color: ${props => props.theme.sidebarHoverBackgroundColor};
    border-left-color: ${props => props.theme.headerBackgroundColor};
  }
`;

const CollapsibleLabel = styled.div`
    flex: 2;
    margin-left: 10px;
`;