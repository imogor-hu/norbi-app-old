import React from 'react';
import {Sidebar, SidebarLink} from './basic/sidebar';
import {Redirect, Route, Switch} from 'react-router';
import {BrowserRouter} from 'react-router-dom';
import {ThemeProvider} from 'styled-components';
import {GlobalStyles} from '../global-styles';
import {defaultTheme} from '../themes/default-theme';
import {styled} from '../themes/theme';
import {Header} from './basic/header';
import {MapMarker} from 'styled-icons/fa-solid';
import {Circle as Circle} from 'styled-icons/fa-regular';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {Collapsible} from './basic/collapsible';
import {Accordion} from './basic/accordion';
import {PlacesListPage} from './pages/places-list-page';

const AppContainer = styled.div`
    display: flex;    
    min-height: 100vh;
`;

const AppContent = styled.div`
    flex-grow: 1;
`;

const CircleIcon = styled(Circle)`
    margin-right: 10px;
`;

const AppContentHeader = styled(Header)``;

export function App()
{
    toast.configure({
        position: "top-right"
    });

    return <ThemeProvider theme={defaultTheme}>
        <>
            <GlobalStyles />
            <BrowserRouter>
                <AppContainer>
                    <Sidebar headerText="Norbi App Admin">
                        <Accordion>
                            <Collapsible label="Létesítmények" icon={MapMarker}>
                                <SidebarLink to="/places"><CircleIcon /> Létesítmények</SidebarLink>
                                <SidebarLink to="/places/new"><CircleIcon /> Új létesítmény létrehozása</SidebarLink>
                            </Collapsible>
                        </Accordion>
                        <SidebarLink to="/places">Létesítmények</SidebarLink>
                    </Sidebar>
                    <AppContent>
                        <AppContentHeader>

                            Some stuff
                        </AppContentHeader>

                        <Switch>
                            <Route exact path="/places" component={PlacesListPage} />
                            <Redirect to="/places" />
                        </Switch>
                    </AppContent>
                </AppContainer>
            </BrowserRouter>
        </>

    </ThemeProvider>;
}