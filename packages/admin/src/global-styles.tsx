import reboot from 'styled-reboot';
import {StyledIconBase} from 'styled-icons/StyledIconBase/StyledIconBase';
import {createGlobalStyle} from './themes/theme';

const rebootCss = reboot();

export const GlobalStyles = createGlobalStyle`
  ${rebootCss}
  
  html {
    font-size: 0.625rem; // 10px by default
  }
  
  body {
    font-size: 1.2rem;
  }
  
  ${StyledIconBase as any /* must cast to any, otherwise it fucks up the typing */} {
    // Make all icons the same size as the text by default
    height: 1em;
    width: 1em;
  }
`;