export default interface Selectable
{
    label: string,
    data: any,
}