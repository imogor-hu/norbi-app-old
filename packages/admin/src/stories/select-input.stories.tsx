import {storiesOf} from '@storybook/react';
import {withKnobs} from '@storybook/addon-knobs';
import {SelectInput} from '../components/form/select-input';
import React from 'react';
import {GlobalStyles} from '../global-styles';

storiesOf('(Multi)SelectInput', module)
    .addDecorator(withKnobs)
    .add('Multi select example', () => <>
            <GlobalStyles />
            <SelectInput
                isMulti={true}
                values={[
                    {label: "First item", data: "some data"},
                    {label: "Second item", data: {random: "stuff"}}
                ]}
                getLabel={(item: any) => <>{item.label}</>}
            />
        </>
    );