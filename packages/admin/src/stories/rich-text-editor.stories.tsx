import {storiesOf} from '@storybook/react';
import {withKnobs} from '@storybook/addon-knobs';
import React, {useState} from 'react';
import {RichTextEditor} from '../components/form/rich-text-editor';
import {GlobalStyles} from '../global-styles';

function RichTextEditorExample()
{
    const [data, setData] = useState('');

    return <>
        <GlobalStyles />
        Current value:
        <pre>{JSON.stringify(data)}</pre>
        <RichTextEditor value={data} onChange={setData} />
    </>;
}

storiesOf('RichTextEditor', module)
    .addDecorator(withKnobs)
    .add('RichTextEditor example', () => <RichTextEditorExample />);