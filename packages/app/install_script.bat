@echo off
echo Removing directories: node_modules, platforms, plugins
rmdir /S /Q node_modules platforms plugins
echo Running yarn
call npm install
echo Adding android platform
call npx cordova platform add android
pause