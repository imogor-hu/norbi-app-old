import {BaseComponent} from '../base-component';
import * as React from 'react';
import loadingSpinner from '../../assets/grid.svg';

export class LoadingIndicator extends BaseComponent
{
    public render(): React.ReactNode
    {
        return <img {...this.props} src={loadingSpinner} />;
    }
}