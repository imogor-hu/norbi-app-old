import React from 'react';
import {BaseComponent} from '../base-component';
import {NavigationBar} from '../navigation-bar/navigation-bar';
import './_main-page.scss';
import { Link } from 'react-router-dom';

export class MainPage extends BaseComponent
{
    public render(): React.ReactNode
    {
        return <div className="main-page">
                    Hello there
                    <Link to='/welcome'>Assdds</Link>
                    <Link to='/auth-page'>Authpage</Link>
                    <Link to='/map'>Map page</Link>
                    <NavigationBar />
        </div>;
    }
}