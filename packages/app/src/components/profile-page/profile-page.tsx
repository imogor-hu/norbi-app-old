import React from 'react';
import { BaseComponent } from '../base-component';
import { NavigationBar } from '../navigation-bar/navigation-bar';



export class ProfilePage extends BaseComponent
{
    public render(): React.ReactNode {
        return <div>
            Profile page
            <NavigationBar />
            </div>;

    }
}