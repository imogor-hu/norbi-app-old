import React from 'react';
import { BaseComponent } from '../base-component';
import './_auth-page.scss';
import { withRouter, Link, Redirect } from 'react-router-dom';
import { tracked } from '../../decorators/tracked';
import {restService} from '../../services/rest-service';
import { PageContent, PageWrapper } from '../../styles/global';
import styled from 'styled-components';
import { firebaseServ } from '../../services/firebase-service';

const LogoWrapper = styled('div')`
    margin-top: 2rem;
    `;

export class AuthPage extends BaseComponent 
{
    @tracked
    private facebookLoginClicked: boolean;

    @tracked
    private googleLoginClicked: boolean;

    @tracked
    private signupClicked: boolean;

    public async authFacebook()   
    {
        console.debug('auth-page authFacebook start');
            
        firebaseServ.loginWithFacebook();
    }

    public async authGoogle()
    {
        console.debug('auth-page authGoogle start');
        
        firebaseServ.loginWithGoogle();
    }

    public render(): React.ReactNode 
    {
        if (this.signupClicked)
            return <Redirect push to='/manual-auth?login=false' />;
        else return <PageWrapper>
                    <PageContent>
                        <LogoWrapper className="logo-wrapper">
                            <img src='http://via.placeholder.com/400x150' />
                        </LogoWrapper>
                        <div className="login-buttons">
                            <button type='button'
                                className='facebook-button'
                                onClick={async () => await this.authFacebook()}
                            >
                                Log in with Facebook
                            </button>
                            <button type='button'
                                className='google-button'
                                onClick={async () => await this.authGoogle()}
                            >
                                Log in with Google
                            </button>
                            <button type='button'
                                className='signup-button'
                                onClick={() => this.signupClicked = true}
                            >
                                Sign up
                            </button>
                            <div className="already-have-account">
                                <span> Already have an account? </span>
                                <Link to='/manual-auth?login=true' > Log in. </Link>
                            </div>
                        </div>
                    </PageContent>
        </PageWrapper>;

    }
}