import * as React from 'react';

const translations = {
    hu: {
        upcomingEvents: <>Közelgő események</>,
        price: ({ price }: { price: number }) => <>{price}Ft</>,
        signUp: <>Regisztrálás</>,
        logIn: <>Bejelentkezés</>,
        alreadyHaveAccount: <>Van már profilod?</>,
        forgotPassword: <>Elfelejtett jelszó?</>,
        acceptedTOS: <>Elfogadom a felhasználási feltételeket</>
    },
    en: {
        upcomingEvents: <>Upcoming events</>,
        price: ({ price }: { price: number }) => <>{price} HUF</>,
        signUp: <>Sign up</>,
        logIn: <>Log in</>,
        alreadyHaveAccount: <>Already have an account?</>,
        forgotPassword: <>Forgot password?</>,
        acceptedTOS: <>I agree to the terms and services</>
    }
};

type TranslationFactory = (props: Dictionary<any>) => JSX.Element;
export type AvailableLanguage = 'hu' | 'en';

export class Translate {
    private locale: AvailableLanguage;

    public setLanguage(locale: AvailableLanguage): void {
        this.locale = locale;
    }

    public getLanguage(): AvailableLanguage {
        return this.locale;
    }

    public translate(key: keyof typeof translations.hu, props?: Dictionary<string | number>): JSX.Element {
        const translation: JSX.Element | TranslationFactory = (translations[this.locale] as Dictionary<any>)[key] || <></>;

        return typeof translation === 'function' ? translation(props || {}) : translation;
    }

    public choose<T>(choices: Dictionary<T>): T {
        return choices[this.locale];
    }
}

export const translate = new Translate();