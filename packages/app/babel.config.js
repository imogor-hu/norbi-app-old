module.exports = function(api) {
    api.cache(true);
    
    return {
        "env": {
            "production": {
                "plugins": [
                    ["babel-plugin-styled-components", {
                        "pure": true,
                        "transpileTemplateLiterals": false
                    }],
                    ["reflective-bind/babel"],
                    "@babel/plugin-syntax-dynamic-import",
                    "@babel/plugin-syntax-object-rest-spread",
                    "@babel/plugin-syntax-jsx",
                    ["@babel/plugin-transform-react-jsx", {
                        "useBuiltins": true
                    }],
                    ["@babel/plugin-transform-runtime", {
                        "useESModules": true
                    }]
                ]
            },
            "development": {
                "plugins": [
                    ["babel-plugin-styled-components", {
                        "displayName": true,
                        "minify": false,
                        "transpileTemplateLiterals": false
                    }],
                    // ["reflective-bind/babel"],
                    "@babel/plugin-syntax-dynamic-import",
                    "@babel/plugin-syntax-object-rest-spread",
                    "@babel/plugin-transform-react-jsx-self",
                    "@babel/plugin-transform-react-jsx-source",
                    // "react-hot-loader/babel",
                    "@babel/plugin-syntax-jsx",
                    ["@babel/plugin-transform-react-jsx", {
                        "useBuiltins": true
                    }],
                    ["@babel/plugin-transform-runtime", {
                        "useESModules": true
                    }]
                ]
            }
        }
    };
};
